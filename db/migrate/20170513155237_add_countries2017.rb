class AddCountries2017 < ActiveRecord::Migration

  def self.up
    countries = ['Israel','Polen','Weissrussland', 'Österreich', 'Armenien', 'Niederlande', 'Moldau', 'Ungarn', 'Italien','Dänemark', 'Portugal', 'Aserbaidschan', 'Kroatien', 'Australien', 'Griechenland', 'Spanien', 'Norwegen', 'Großbritannien', 'Zypern', 'Rumänien', 'Deutschland', 'Ukraine', 'Belgien', 'Schweden', 'Bulgarien', 'Frankreich']
    for country in countries
        Country.create(:name=>country)
    end
  end

  def self.down
    Country.delete_all
  end
end