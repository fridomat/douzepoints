class AddIndexToVotes < ActiveRecord::Migration
  def change
  	add_index :votes, [:country_id, :user_id]
  end
end
