class Vote < ActiveRecord::Base
	belongs_to :country
	belongs_to :user

	scope :has_points, -> { where("points is not null") }
  	scope :has_no_points, -> { where(points: nil) } 

  	def points?
  	  !points.blank?
    end

  	def self.get_points_for_country_and_user(country, user)
  		Vote.where(country: country, user: user).last
  	end 

    def give_points(amount, country)
		vote = Vote.find_or_create_by(user: self, country: country) 
      	vote.points = amount
      vote.save
    end

end
