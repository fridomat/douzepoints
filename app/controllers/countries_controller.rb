class CountriesController < ApplicationController



	def index
		@countries = Country.all
		@countries = Country.all.sort {|a,b| a.total_score <=> b.total_score}.reverse
	end

	def show
		@country = Country.find(params[:id])
	end

	def new
	  	@country = Country.new
	  end

	def create  
	  	@country = Country.new(country_params)  
  	end


private
	def country_params
      params.require(:country).permit(:name)
    end


end
